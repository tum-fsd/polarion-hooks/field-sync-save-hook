# Work Item Field Sync Save Hook

The latest jar file is available in the [releases section](../../releases).

## Introduction

Sometimes a work item field is based on fields of linked work items. For such a use case, Polarion offers calculated fields. These fields then depend on fields of the linked parent or itself.
Unfortunately, this feature is limited to the data types integer, float, currency and duration for the fields initialEstimate, timeSpent and remainingEstimate.

Because of the limitations of the calculated fields feature, it can sometimes be the easier option to sync the content of fields between linked work items. This allows to continue to use all the already existing widgets and scripts since the information is part of the work item and does not need to be calculated in the background, which would not work with the existing widgets and scripts.

NOTE: In general, it is a bad idea to sync information from one place to another and thus have duplicate information instead of dynamically compiling that information from the source. When the syncing mechanism is automated and combined with well-thought-out limited write permissions, the approach does not violate the single source of truth philosophy. Data is only changed in one work item but propagated to further work items.

The motivation for the implementation of this hook are tasks in our project management on Polarion that are linked to higher level clusters. For those clusters, we want to know when the first task will start and the last task will end / be due.
By syncing the earliest start day and last due date of all linked tasks, this information is directly available with the cluster work item and usable in all already existing widgets.
Since the calculated fields feature does not support the data type `date`, we needed to implement something on our own and decided to use the [Work Item Action Interceptor Framework](https://extensions.polarion.com/extensions/146-work-item-action-interceptor-framework) for this.

Supported field types are (source and target type must match):
- Text
- String
- Duration
- Date
- Date time
- Integer
- Float
- Boolean
- Currency
- Time

CAUTION: Carefully follow the configuration instructions below! Otherwise you might end up with infinite loops of sync operations and render your Polarion server unusable until you change the configuration!

## License

Download and use of the tool is free. By downloading the tool, you accept the license terms.

See [NOTICE.md](NOTICE.md) for licensing details.


## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) for contribution guidelines.


## Download & Installation

### Installation:

1. Have the Work Item Action Interceptor Framework (see [Installation Requirements](#Installation-Requirements)) installed and follow the instructions of that extension.

1. Go to the [releases section](../../releases) of this repository and download the latest version of the hook.

1. Copy the jar file in the `hooks` folder of the interceptor framework on your Polarion server.

1. Add the [configuration properties](#Configuration-Properties) in the `hooks.properties` file of the interceptor framework.

1. Follow the instructions of the interceptor framework on how to reload the hooks and configuration properties.

### Configuration Properties:

This hook has the following syntax for the configuration properties:

`FieldSync.<Source Project ID>.<Source WorkItem Type ID>.<Source Field ID>[.<optional string for uniqueness e.g. 1>]=<Target Project ID>,<Target WorkItem Type ID>,<Target Field ID>,<Link Role ID>,<Link Direction>,<Sync Mode>` where

- `<Source Project ID>` and `<Source WorkItem Type ID>` can be replaced by a `*` so that the rule applies for all projects and/or work item types. Keep in mind that all matching rules will be applied. More specific rules don't override more generic rules.
- `<Source/Target Field ID>` is a field of the data type `String`, `Text`, `Rich Text`, `Integer`, `Float`, `Currency`, `Time`, `Date`, `Date time`, `Duration`, `Boolean` where the data type of the source and the target must match.
- `<Link Direction>` is either `direct` or `back`
- `<Sync Mode>` is either `min`, `max` or `single`.

For `single`, only the updated source is synced to the target. For `min` and `max`, the minimum or maximum value of all source work items linked to the target is synced to the target. In the figure below this means: 

- Sync mode is `single` and `Source 1` is changed: Only the value of `Source 1` will be synced to `Target`.
- Sync mode is `min` or `max` and `Source 1` is changed: The minimum or maximum value from either `Source 1` or `Source 2` or `Source 3` will be synced to `Target`.

```mermaid
graph TD
    B[Source 1] --> A[Target]
    C[Source 2] --> A
    D[Source 3] --> A
```

You absolutely need to prevent to create direct or indirect loops between two work items as shown in the following figure. Otherwise, this will result in infinite syncing loops.

```mermaid
graph TD
    A[A] --> B[B]
    B --> C[C]
    C --> A
    D(Never do this!)
```

#### Syntax Examples:

`FieldSync.DummyProject.systemRequirement.dueDate=DummyProject,package,dueDate,belongs_to,direct,max`

or 

`FieldSync.DummyProject.systemRequirement.dueDate.1=DummyProject,package,dueDate,belongs_to,direct,max`

`FieldSync.DummyProject.systemRequirement.dueDate.2=DummyProject,anotherWorkItem,dueDate,belongs_to,direct,max`

or

`FieldSync.*.systemRequirement.dueDate=DummyProject,package,dueDate,belongs_to,direct,max`

#### Syncing Examples:


In the second example, the same source field is linked to two different target fields.

In the third example, the rule applies for system requirements in all projects on the server.

### Uninstall:

- Remove the jar file from the `hooks` folder of the interceptor framework, remove the corresponding entries in the `hooks.properties` file and reload the hooks and properties in the `Action Interceptor Manager` wiki page.

### Installation Requirements:

- [Work Item Action Interceptor Framework](https://extensions.polarion.com/extensions/146-work-item-action-interceptor-framework) version 3.0.3 and later

- Polarion version 3.19.1 and later