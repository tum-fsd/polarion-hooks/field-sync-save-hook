package com.tulrfsd.polarion.actionhooks.save.fieldsync;

public enum SyncMode {
  MIN,
  MAX,
  SINGLE
}
