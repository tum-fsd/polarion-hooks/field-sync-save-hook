package com.tulrfsd.polarion.actionhooks.save.fieldsync;

public class SyncConfig {
  
  private String sourceProjectId;
  private String sourceWorkItemTypeId;
  private String sourceFieldId;
  private String targetProjectId;
  private String targetWorkItemTypeId;
  private String targetFieldId;
  private String linkRoleId;
  private LinkDirection linkDirection;
  private SyncMode syncMode;
  
  SyncConfig(String sourceProjectId, String sourceWorkItemTypeId, String sourceFieldId, String targetProjectId, String targetWorkItemTypeId, String targetFieldId, String linkRoleId, String linkDirection, String syncMode) {
    this.sourceProjectId = sourceProjectId;
    this.sourceWorkItemTypeId = sourceWorkItemTypeId;
    this.sourceFieldId = sourceFieldId;
    this.targetProjectId = targetProjectId;
    this.targetWorkItemTypeId = targetWorkItemTypeId;
    this.targetFieldId = targetFieldId;
    this.linkRoleId = linkRoleId;
    switch (linkDirection.toLowerCase()) {
      case "direct":
        this.setLinkDirection(LinkDirection.DIRECT);
        break;
      case "back":
        this.setLinkDirection(LinkDirection.BACK);
        break;
      default:
        this.setLinkDirection(LinkDirection.DIRECT);
        break;
    }
    switch (syncMode.toLowerCase()) {
      case "min":
        this.syncMode = SyncMode.MIN;
        break;
      case "max":
        this.syncMode = SyncMode.MAX;
        break;
      case "single":
        this.syncMode = SyncMode.SINGLE;
        break;
      default:
        this.syncMode = SyncMode.SINGLE;
        break;
    }
    
  }

  public String getTargetProjectId() {
    return targetProjectId;
  }

  void setTargetProjectId(String targetProjectId) {
    this.targetProjectId = targetProjectId;
  }

  public String getTargetWorkItemTypeId() {
    return targetWorkItemTypeId;
  }

  void setTargetWorkItemTypeId(String targetWorkItemTypeId) {
    this.targetWorkItemTypeId = targetWorkItemTypeId;
  }
  
  public String getTargetFieldId() {
    return targetFieldId;
  }

  void setTargetFieldId(String targetFieldId) {
    this.targetFieldId = targetFieldId;
  }

  public String getLinkRoleId() {
    return linkRoleId;
  }

  void setLinkRoleId(String linkRoleId) {
    this.linkRoleId = linkRoleId;
  }
  
  public LinkDirection getLinkDirection() {
    return linkDirection;
  }

  public void setLinkDirection(LinkDirection linkDirection) {
    this.linkDirection = linkDirection;
  }

  public SyncMode getSyncMode() {
    return syncMode;
  }

  void setSyncMode(SyncMode syncMode) {
    this.syncMode = syncMode;
  }

  public String getSourceWorkItemTypeId() {
    return sourceWorkItemTypeId;
  }

  void setSourceWorkItemTypeId(String sourceWorkItemTypeId) {
    this.sourceWorkItemTypeId = sourceWorkItemTypeId;
  }

  public String getSourceProjectId() {
    return sourceProjectId;
  }

  void setSourceProjectId(String sourceProjectId) {
    this.sourceProjectId = sourceProjectId;
  }

  public String getSourceFieldId() {
    return sourceFieldId;
  }

  void setSourceFieldId(String sourceFieldId) {
    this.sourceFieldId = sourceFieldId;
  }



}